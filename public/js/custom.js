$(document).ready(function () {
    var hostname = "http://"+$(location).attr('hostname');

    var array = $("td[name='subtotal[]']")
        .map(function(){return $(this).text();}).get();
    var subtotal=0;
    for(var i=0; i<=array.length-1; i++){
        subtotal = subtotal+parseInt(array[i]);

    }
    $("input[name='total_venta']").val(subtotal);

    $(function () {
        $(document).on('click', '.borrar_producto', function (event) {
            event.preventDefault();

            $(this).closest('tr').remove();

            var array2 = $("td[name='subtotal[]']")
                .map(function(){return $(this).text();}).get();

            var subtotal=0;
            for(var i=0; i<=array2.length-1; i++){
                subtotal = subtotal+parseInt(array2[i]);

            }
            $("input[name='total_venta']").val(subtotal);

        });
    });

    var url = window.location.pathname.split('/');

    var nuevoPath = "http://localhost";
    for (i = 0; i < 3; i++) {

        nuevoPath += url[i];
        if(i == 2){break;}
        nuevoPath += "/";
    }

// Obtiene la ubicación de la cadena href
    $('ul.nav a[href="'+ nuevoPath +'"]').parent().addClass('active');

    $('ul.nav a').filter(function() {
        return this.href == nuevoPath;
    }).parent().addClass('active');

    //cambiar titulo de paginas
    if(url[3] == null){
        $("#titulo").text(url[2].toUpperCase());
    }else{
        $("#titulo").text(url[3].toUpperCase());
    }



    $("#mensaje").fadeTo(1500, 500).slideUp(500, function(){
        $("#mensaje").slideUp(500);
    });

    $("#agregar_producto").on('click',function () {

        var producto_id = $( "select[name='producto_id'] option:selected" ).val();
        var producto_cantidad = $("#producto_cantidad").val();

        var total = 0;

        $.ajax({

            url:hostname+"/gusatest/productos/verProductoAjax/"+producto_id+"/"+producto_cantidad,

            dataType: "html",
            success: function (msg) {
                $("tbody[name='mas_producto']").append(msg);

                var array = $("td[name='subtotal[]']")
                    .map(function(){return $(this).text();}).get();
                var subtotal=0;
                for(var i=0; i<=array.length-1; i++){
                    subtotal = subtotal+parseInt(array[i]);

                }
                $("input[name='total_venta']").val(subtotal);


                $(function () {
                    $(document).on('click', '.borrar_producto', function (event) {
                        event.preventDefault();

                        $(this).closest('tr').remove();

                        var array2 = $("td[name='subtotal[]']")
                            .map(function(){return $(this).text();}).get();

                        var subtotal=0;
                        for(var i=0; i<=array2.length-1; i++){
                            subtotal = subtotal+parseInt(array2[i]);

                        }
                        $("input[name='total_venta']").val(subtotal);

                    });
                });
            }
        });
    });
});
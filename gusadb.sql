drop schema if exists gusadb;
create schema gusadb;
use gusadb;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: gusadb
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `asesores`
--

DROP TABLE IF EXISTS `asesores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asesores` (
  `num_colaborador` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `activo` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`num_colaborador`)
) ENGINE=InnoDB AUTO_INCREMENT=13044 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asesores`
--

LOCK TABLES `asesores` WRITE;
/*!40000 ALTER TABLE `asesores` DISABLE KEYS */;
INSERT INTO `asesores` VALUES (1232,'Sergio Sanchez Sanchez','Centro #44, Col. Nuevo Sol, Solidaridad, QR','984123456','test2@email.com',1),(1245,'Marco García','Calle Tamarindo, Solidaridad, QR','99812346','test3@email.com',1),(8965,'Guillermo Ochoa Solis','Centro #44, Col. Nuevo Sol, Solidaridad, QR','984123456','test4@email.com',1),(13043,'Raúl Rodriguez Pérez','test dirección # 44 col. escondido','984123456','test@email.com',1);
/*!40000 ALTER TABLE `asesores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'FLY & BUY','Plan de tiempo compartido'),(2,'Club Vacacional Nacional','Club Vacacional Nacional'),(3,'Inversión express','Bienes raíces'),(4,'Intercambio de semanas','Intercambio de semanas por compra de tiempo compartido');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `nacionalidad` varchar(45) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Pablo Ramirez','PlayaCar Fase 2, Solidaridad, QR','984123456','Méxicana',32,'test1@email.com'),(2,'Juan Pablo Mendoza','Cerrada # 456 Col. Centro Playa del Carmen, Q.R','984123456','Méxicana',23,'test@email.com'),(3,'Sandy García Sanchez','Centro #44, Col. Nuevo Sol, Solidaridad, QR','9841222','Méxicana',18,'test3@email.com'),(4,'Yanely Uitz UC','Calle Tamarindo, Solidaridad, QR','99812346','Méxicana',25,'test4@email.com'),(5,'Miguel Ramirez','Centro #44, Col. Nuevo Sol, Solidaridad, QR','984123456','Méxicana',29,'test4@email.com');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `precio` varchar(45) DEFAULT NULL,
  `activo` tinyint(4) DEFAULT NULL,
  `categoria_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_productos_categorias1_idx` (`categoria_id`),
  CONSTRAINT `fk_productos_categorias1` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'Fee Simple','In this system you purchase an actual deeded ','25000',1,1),(2,'Ocean Residences','Vuele y alójese gratis al comprar en The Ocea','15000',1,1),(3,'Membresía Tiempo Compartido Hotel Vidanta','- Vitalicia y heredable\r\n- 6 semanas al año!\r\n- Acceso a todas las temporadas del año (Semana Santa/Pascua y Navidad/Año\r\nNuevo con promoción)\r\n- Hoteles: Grand Mayan, Mayan Palace, Bliss y Sea Garden (Grand Bliss y Grand Luxxe con promoción)\r\n- Destinos:','420000',1,2);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) DEFAULT NULL,
  `clientes_id` int(11) NOT NULL,
  `detalles` blob,
  `total` float DEFAULT NULL,
  `asesores_id` int(11) NOT NULL,
  `comentarios` varchar(190) DEFAULT NULL,
  `fecha_creacion` timestamp NULL DEFAULT NULL,
  `fecha_actualizacion` timestamp NULL DEFAULT NULL,
  `estatus` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ventas_clientes1_idx` (`clientes_id`),
  KEY `fk_ventas_asesores1_idx` (`asesores_id`),
  CONSTRAINT `fk_ventas_asesores1` FOREIGN KEY (`asesores_id`) REFERENCES `asesores` (`num_colaborador`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_ventas_clientes1` FOREIGN KEY (`clientes_id`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas`
--

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
INSERT INTO `ventas` VALUES (1,'Titulo nuevo',1,'[{\"products_id\":\"1\",\"products_nombre\":\"Fee Simple\",\"products_descripcion\":\"In this system you purchase an actual deeded \",\"products_precio\":\"25000\",\"products_cantidad\":\"1\",\"products_subtotal\":\"25000\"},{\"products_id\":\"1\",\"products_nombre\":\"Fee Simple\",\"products_descripcion\":\"In this system you purchase an actual deeded \",\"products_precio\":\"25000\",\"products_cantidad\":\"1\",\"products_subtotal\":\"25000\"}]',50000,13043,'Recien casado','2019-02-11 05:00:00','2019-02-11 05:00:00','Vendida'),(2,'Nueva venta 2',4,'[{\"products_id\":\"2\",\"products_nombre\":\"Ocean Residences\",\"products_descripcion\":\"Vuele y al\\u00f3jese gratis al comprar en The Ocea\",\"products_precio\":\"15000\",\"products_cantidad\":\"2\",\"products_subtotal\":\"30000\"}]',30000,1245,'nuevos comentarios','2019-02-12 05:00:00','2019-02-11 05:00:00','Vendida'),(3,'Nueva venta 3',4,'[{\"products_id\":\"3\",\"products_nombre\":\"Membres\\u00eda Tiempo Compartido Hotel Vidanta\",\"products_descripcion\":\"- Vitalicia y heredable\\r\\n- 6 semanas al a\\u00f1o!\\r\\n- Acceso a todas las temporadas del a\\u00f1o (Semana Santa\\/Pascua y Navidad\\/A\\u00f1o\\r\\nNuevo con promoci\\u00f3n)\\r\\n- Hoteles: Grand Mayan, Mayan Palace, Bliss y Sea Garden (Grand Bliss y Grand Luxxe con promoci\\u00f3n)\\r\\n- Destinos:\",\"products_precio\":\"420000\",\"products_cantidad\":\"2\",\"products_subtotal\":\"840000\"}]',840000,1232,'nuevos comentarios','2019-02-11 05:00:00','2019-02-11 05:00:00','Pendiente'),(4,'Nueva venta 4',3,'[{\"products_id\":\"1\",\"products_nombre\":\"Fee Simple\",\"products_descripcion\":\"In this system you purchase an actual deeded \",\"products_precio\":\"25000\",\"products_cantidad\":\"3\",\"products_subtotal\":\"75000\"}]',75000,8965,'nuevos comentarios','2019-02-11 05:00:00','2019-02-11 05:00:00','Vendida'),(5,'Nueva venta 5',1,'[{\"products_id\":\"3\",\"products_nombre\":\"Membres\\u00eda Tiempo Compartido Hotel Vidanta\",\"products_descripcion\":\"- Vitalicia y heredable\\r\\n- 6 semanas al a\\u00f1o!\\r\\n- Acceso a todas las temporadas del a\\u00f1o (Semana Santa\\/Pascua y Navidad\\/A\\u00f1o\\r\\nNuevo con promoci\\u00f3n)\\r\\n- Hoteles: Grand Mayan, Mayan Palace, Bliss y Sea Garden (Grand Bliss y Grand Luxxe con promoci\\u00f3n)\\r\\n- Destinos:\",\"products_precio\":\"420000\",\"products_cantidad\":\"3\",\"products_subtotal\":\"1260000\"}]',1260000,13043,'nuevos comentarios','2019-02-11 05:00:00','2019-02-11 05:00:00','Vendida'),(6,'Nueva venta 6',5,'[{\"products_id\":\"2\",\"products_nombre\":\"Ocean Residences\",\"products_descripcion\":\"Vuele y al\\u00f3jese gratis al comprar en The Ocea\",\"products_precio\":\"15000\",\"products_cantidad\":\"5\",\"products_subtotal\":\"75000\"},{\"products_id\":\"3\",\"products_nombre\":\"Membres\\u00eda Tiempo Compartido Hotel Vidanta\",\"products_descripcion\":\"- Vitalicia y heredable\\r\\n- 6 semanas al a\\u00f1o!\\r\\n- Acceso a todas las temporadas del a\\u00f1o (Semana Santa\\/Pascua y Navidad\\/A\\u00f1o\\r\\nNuevo con promoci\\u00f3n)\\r\\n- Hoteles: Grand Mayan, Mayan Palace, Bliss y Sea Garden (Grand Bliss y Grand Luxxe con promoci\\u00f3n)\\r\\n- Destinos:\",\"products_precio\":\"420000\",\"products_cantidad\":\"5\",\"products_subtotal\":\"2100000\"}]',2175000,8965,'nuevos comentarios','2019-02-11 05:00:00','2019-02-11 05:00:00','Vendida'),(7,'Nueva venta 7',4,'[{\"products_id\":\"3\",\"products_nombre\":\"Membres\\u00eda Tiempo Compartido Hotel Vidanta\",\"products_descripcion\":\"- Vitalicia y heredable\\r\\n- 6 semanas al a\\u00f1o!\\r\\n- Acceso a todas las temporadas del a\\u00f1o (Semana Santa\\/Pascua y Navidad\\/A\\u00f1o\\r\\nNuevo con promoci\\u00f3n)\\r\\n- Hoteles: Grand Mayan, Mayan Palace, Bliss y Sea Garden (Grand Bliss y Grand Luxxe con promoci\\u00f3n)\\r\\n- Destinos:\",\"products_precio\":\"420000\",\"products_cantidad\":\"1\",\"products_subtotal\":\"420000\"},{\"products_id\":\"1\",\"products_nombre\":\"Fee Simple\",\"products_descripcion\":\"In this system you purchase an actual deeded \",\"products_precio\":\"25000\",\"products_cantidad\":\"1\",\"products_subtotal\":\"25000\"},{\"products_id\":\"2\",\"products_nombre\":\"Ocean Residences\",\"products_descripcion\":\"Vuele y al\\u00f3jese gratis al comprar en The Ocea\",\"products_precio\":\"15000\",\"products_cantidad\":\"1\",\"products_subtotal\":\"15000\"}]',460000,8965,'nuevos comentarios','2019-02-11 05:00:00','2019-02-11 05:00:00','Vendida'),(8,'Nueva venta 8',3,'[{\"products_id\":\"3\",\"products_nombre\":\"Membres\\u00eda Tiempo Compartido Hotel Vidanta\",\"products_descripcion\":\"- Vitalicia y heredable\\r\\n- 6 semanas al a\\u00f1o!\\r\\n- Acceso a todas las temporadas del a\\u00f1o (Semana Santa\\/Pascua y Navidad\\/A\\u00f1o\\r\\nNuevo con promoci\\u00f3n)\\r\\n- Hoteles: Grand Mayan, Mayan Palace, Bliss y Sea Garden (Grand Bliss y Grand Luxxe con promoci\\u00f3n)\\r\\n- Destinos:\",\"products_precio\":\"420000\",\"products_cantidad\":\"1\",\"products_subtotal\":\"420000\"}]',420000,8965,'nuevos comentarios','2019-02-11 05:00:00','2019-02-11 05:00:00','Cancelada'),(9,'Nueva venta 9',2,'[{\"products_id\":\"3\",\"products_nombre\":\"Membres\\u00eda Tiempo Compartido Hotel Vidanta\",\"products_descripcion\":\"- Vitalicia y heredable\\r\\n- 6 semanas al a\\u00f1o!\\r\\n- Acceso a todas las temporadas del a\\u00f1o (Semana Santa\\/Pascua y Navidad\\/A\\u00f1o\\r\\nNuevo con promoci\\u00f3n)\\r\\n- Hoteles: Grand Mayan, Mayan Palace, Bliss y Sea Garden (Grand Bliss y Grand Luxxe con promoci\\u00f3n)\\r\\n- Destinos:\",\"products_precio\":\"420000\",\"products_cantidad\":\"3\",\"products_subtotal\":\"1260000\"},{\"products_id\":\"1\",\"products_nombre\":\"Fee Simple\",\"products_descripcion\":\"In this system you purchase an actual deeded \",\"products_precio\":\"25000\",\"products_cantidad\":\"3\",\"products_subtotal\":\"75000\"}]',1335000,1232,'nuevos comentarios','2019-02-11 05:00:00','2019-02-11 05:00:00','Vendida'),(10,'Nueva venta 10',1,'[{\"products_id\":\"3\",\"products_nombre\":\"Membres\\u00eda Tiempo Compartido Hotel Vidanta\",\"products_descripcion\":\"- Vitalicia y heredable\\r\\n- 6 semanas al a\\u00f1o!\\r\\n- Acceso a todas las temporadas del a\\u00f1o (Semana Santa\\/Pascua y Navidad\\/A\\u00f1o\\r\\nNuevo con promoci\\u00f3n)\\r\\n- Hoteles: Grand Mayan, Mayan Palace, Bliss y Sea Garden (Grand Bliss y Grand Luxxe con promoci\\u00f3n)\\r\\n- Destinos:\",\"products_precio\":\"420000\",\"products_cantidad\":\"1\",\"products_subtotal\":\"420000\"}]',420000,8965,'nuevos comentarios','2019-02-11 05:00:00','2019-02-11 05:00:00','Vendida'),(11,'Nueva venta 11',3,'[{\"products_id\":\"2\",\"products_nombre\":\"Ocean Residences\",\"products_descripcion\":\"Vuele y al\\u00f3jese gratis al comprar en The Ocea\",\"products_precio\":\"15000\",\"products_cantidad\":\"1\",\"products_subtotal\":\"15000\"},{\"products_id\":\"2\",\"products_nombre\":\"Ocean Residences\",\"products_descripcion\":\"Vuele y al\\u00f3jese gratis al comprar en The Ocea\",\"products_precio\":\"15000\",\"products_cantidad\":\"1\",\"products_subtotal\":\"15000\"}]',30000,1245,'nuevos comentarios','2019-02-11 05:00:00','2019-02-11 05:00:00','Pendiente');
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'gusadb'
--

--
-- Dumping routines for database 'gusadb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-11  2:56:58

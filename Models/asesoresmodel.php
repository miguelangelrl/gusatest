<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 12:30 PM
 */

require 'Entities/entityasesor.php';

class AsesoresModel extends Model{
    public function __construct(){
        parent::__construct();
    }
    public function get(){
        $items = [];
        try{
            $query = $this->db->connect()->query('SELECT * FROM asesores order by num_colaborador DESC');

            while($row = $query->fetch()){
                $item = new EntityAsesor();

                $item->num_colaborador = $row['num_colaborador'];
                $item->nombre = $row['nombre'];
                $item->direccion    = $row['direccion'];
                $item->telefono  = $row['telefono'];
                $item->email  = $row['email'];
                $item->activo  = $row['activo'];

                array_push($items, $item);
            }
            return $items;
        }catch(PDOException $e){
            return [];
        }
    }

    public function getById($id){
        $item = new EntityAsesor(); // instanciando objeto de controlador
        try{
            $query = $this->db->connect()->prepare('SELECT * FROM asesores WHERE num_colaborador = :id');
            $query->execute(['id' => $id]);

            while($row = $query->fetch()){

                $item->num_colaborador = $row['num_colaborador'];
                $item->nombre = $row['nombre'];
                $item->direccion    = $row['direccion'];
                $item->telefono  = $row['telefono'];
                $item->email  = $row['email'];
                $item->activo  = $row['activo'];
            }
            return $item;
        }catch(PDOException $e){
            return null;
        }
    }

    public function insert($datos){
        // insertar


        $query = $this->db->connect()
            ->prepare('INSERT INTO asesores (num_colaborador, nombre, direccion, telefono, email, activo) 
                                VALUES(:num_colaborador, :nombre, :direccion, :telefono, :email, :activo)');
        try{
            $query->execute([
                'num_colaborador' => $datos['num_colaborador'],
                'nombre' => $datos['nombre'],
                'direccion' => $datos['direccion'],
                'telefono' => $datos['telefono'],
                'email' => $datos['email'],
                'activo' => $datos['activo']
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }


    }

    public function update($item){
//                echo '<pre>';
//        print_r($item['email']);
//        exit;
        $query = $this->db->connect()
            ->prepare('UPDATE asesores SET                       
                      nombre = :nombre, 
                      direccion = :direccion,
                      telefono = :telefono,                     
                      email = :email,
                      activo = :activo
                      WHERE num_colaborador = :num_colaborador');
        try{
            $query->execute([
                'num_colaborador' => $item['num_colaborador'],
                'nombre' => $item['nombre'],
                'direccion' => $item['direccion'],
                'telefono' => $item['telefono'],
                'email' => $item['email'],
                'activo' => $item['activo']
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    public function delete($id){
        $query = $this->db->connect()->prepare('DELETE FROM asesores WHERE num_colaborador = :id');
        try{
            $query->execute([
                'id' => $id
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
}
?>
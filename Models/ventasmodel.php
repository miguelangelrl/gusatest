<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 12:30 PM
 */

require 'Entities/entityventa.php';

class VentasModel extends Model{
    public function __construct(){
        parent::__construct();
    }
    public function get(){
        $items = [];
        try{
            $query = $this->db->connect()
                ->query('SELECT 
                                        ventas.*,
                                        cli.nombre AS clientes_nombre,
                                        ase.nombre AS asesores_nombre
                                    FROM
                                        ventas
                                            JOIN
                                        clientes AS cli ON ventas.clientes_id = cli.id
                                            JOIN
                                        asesores AS ase ON ventas.asesores_id = ase.num_colaborador
                                    ORDER BY ventas.id DESC'
                        );

            while($row = $query->fetch()){
                $item = new EntityVenta();
                $item->id = $row['id'];
                $item->titulo = $row['titulo'];
                $item->clientes_id    = $row['clientes_id'];
                $item->clientes_nombre    = $row['clientes_nombre'];
                $item->detalles  = $row['detalles'];
                $item->total  = $row['total'];
                $item->asesores_id  = $row['asesores_id'];
                $item->asesores_nombre  = $row['asesores_nombre'];
                $item->comentarios  = $row['comentarios'];
                $item->fecha_creacion  = $row['fecha_creacion'];
                $item->fecha_actualizacion  = $row['fecha_actualizacion'];
                $item->estatus  = $row['estatus'];

                array_push($items, $item);
            }
            return $items;
        }catch(PDOException $e){
            return [];
        }
    }
    public function getLimit($limit){
        $items = [];
        try{
            $query = $this->db->connect()
                ->query('SELECT 
                                        ventas.*,
                                        cli.nombre AS clientes_nombre,
                                        ase.nombre AS asesores_nombre
                                    FROM
                                        ventas
                                            JOIN
                                        clientes AS cli ON ventas.clientes_id = cli.id
                                            JOIN
                                        asesores AS ase ON ventas.asesores_id = ase.num_colaborador
                                    ORDER BY ventas.id DESC LIMIT '.$limit.''
                );

            while($row = $query->fetch()){
                $item = new EntityVenta();
                $item->id = $row['id'];
                $item->titulo = $row['titulo'];
                $item->clientes_id    = $row['clientes_id'];
                $item->clientes_nombre    = $row['clientes_nombre'];
                $item->detalles  = $row['detalles'];
                $item->total  = $row['total'];
                $item->asesores_id  = $row['asesores_id'];
                $item->asesores_nombre  = $row['asesores_nombre'];
                $item->comentarios  = $row['comentarios'];
                $item->fecha_creacion  = $row['fecha_creacion'];
                $item->fecha_actualizacion  = $row['fecha_actualizacion'];
                $item->estatus  = $row['estatus'];

                array_push($items, $item);
            }

            return $items;
        }catch(PDOException $e){
            return [];
        }
    }
    public function getById($id){
        $item = new EntityVenta();
        try{
            $query = $this->db->connect()->prepare('SELECT * FROM ventas WHERE id = :id');
            $query->execute(['id' => $id]);

            while($row = $query->fetch()){

                $item->id = $row['id'];
                $item->titulo = $row['titulo'];
                $item->clientes_id    = $row['clientes_id'];
                $item->detalles  = $row['detalles'];
                $item->total  = $row['total'];
                $item->asesores_id  = $row['asesores_id'];
                $item->comentarios  = $row['comentarios'];
                $item->fecha_creacion  = $row['fecha_creacion'];
                $item->estatus  = $row['estatus'];
            }
            return $item;
        }catch(PDOException $e){
            return null;
        }
    }

    public function insert($datos){

        $query = $this->db->connect()
            ->prepare('INSERT INTO ventas (titulo, clientes_id, detalles, total, asesores_id, comentarios, fecha_creacion, fecha_actualizacion, estatus) 
                                VALUES(:titulo, :clientes_id, :detalles, :total, :asesores_id, :comentarios, :fecha_creacion, :fecha_actualizacion, :estatus)');
        try{
            $query->execute([
                'titulo' => $datos['titulo'],
                'clientes_id' => $datos['clientes_id'],
                'detalles' => $datos['detalles'],
                'total' => $datos['total'],
                'asesores_id' => $datos['asesores_id'],
                'comentarios' => $datos['comentarios'],
                'fecha_creacion' => $datos['fecha_creacion'],
                'fecha_actualizacion' => $datos['fecha_actualizacion'],
                'estatus' => $datos['estatus']
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }

    public function update($item){

        $query = $this->db->connect()
            ->prepare('UPDATE ventas SET 
                      titulo = :titulo, 
                      clientes_id = :clientes_id,
                      detalles = :detalles,
                      total = :total, 
                      asesores_id = :asesores_id,
                      comentarios = :comentarios,
                      fecha_actualizacion = :fecha_actualizacion,
                      estatus = :estatus
                      WHERE id = :id');
        try{
            $query->execute([
                'id' => $item['id'],
                'titulo' => $item['titulo'],
                'clientes_id' => $item['clientes_id'],
                'detalles' => $item['detalles'],
                'total' => $item['total'],
                'asesores_id' => $item['asesores_id'],
                'comentarios' => $item['comentarios'],
                'fecha_actualizacion' => date('Y/m/d'),
                'estatus' => $item['estatus']
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    public function delete($id){
        $query = $this->db->connect()->prepare('DELETE FROM ventas WHERE id = :id');
        try{
            $query->execute([
                'id' => $id
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
}
?>
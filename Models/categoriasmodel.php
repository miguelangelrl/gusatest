<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 12:30 PM
 */

require 'Entities/entitycategoria.php';

class CategoriasModel extends Model{
    public function __construct(){
        parent::__construct();
    }
    public function get(){
        $items = [];
        try{
            $query = $this->db->connect()->query('SELECT * FROM categorias order by id DESC');

            while($row = $query->fetch()){
                $item = new EntityCategoria();
                $item->id = $row['id'];
                $item->nombre = $row['nombre'];
                $item->descripcion    = $row['descripcion'];

                array_push($items, $item);
            }
            return $items;
        }catch(PDOException $e){
            return [];
        }
    }
    public function getById($id){
        $item = new EntityCategoria();
        try{
            $query = $this->db->connect()->prepare('SELECT * FROM categorias WHERE id = :id');
            $query->execute(['id' => $id]);

            while($row = $query->fetch()){

                $item->id = $row['id'];
                $item->nombre = $row['nombre'];
                $item->descripcion    = $row['descripcion'];
            }
            return $item;
        }catch(PDOException $e){
            return null;
        }
    }

    public function insert($datos){
        // insertar


        $query = $this->db->connect()
            ->prepare('INSERT INTO categorias (nombre, descripcion) 
                                VALUES(:nombre, :descripcion)');
        try{
            $query->execute([
                'nombre' => $datos['nombre'],
                'descripcion' => $datos['descripcion']
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }


    }

    public function update($item){

        $query = $this->db->connect()
            ->prepare('UPDATE categorias SET 
                      nombre = :nombre, 
                      descripcion = :descripcion                      
                      WHERE id = :id');
        try{
            $query->execute([
                'id' => $item['id'],
                'nombre' => $item['nombre'],
                'descripcion' => $item['descripcion']
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    public function delete($id){
        $query = $this->db->connect()->prepare('DELETE FROM categorias WHERE id = :id');
        try{
            $query->execute([
                'id' => $id
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
}
?>
<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 12:30 PM
 */

require 'Entities/entityproducto.php';

class ProductosModel extends Model{
    public function __construct(){
        parent::__construct();
    }
    public function get(){
        $items = [];
        try{
            $query = $this->db->connect()->query('SELECT productos.*, c.nombre as categoria_nombre FROM productos join categorias as c on productos.categoria_id = c.id  order by productos.id DESC');

            while($row = $query->fetch()){
                $item = new EntityProducto();
                $item->id = $row['id'];
                $item->nombre = $row['nombre'];
                $item->descripcion    = $row['descripcion'];
                $item->precio  = $row['precio'];
                $item->activo  = $row['activo'];
                $item->categoria_id  = $row['categoria_id'];
                $item->categoria_nombre = $row['categoria_nombre'];

                array_push($items, $item);
            }
            return $items;
        }catch(PDOException $e){
            return [];
        }
    }

    public function getById($id){
        $item = new EntityProducto();
        try{
            $query = $this->db->connect()->prepare('SELECT * FROM productos WHERE id = :id');
            $query->execute(['id' => $id]);

            while($row = $query->fetch()){
                $item->id = $row['id'];
                $item->nombre = $row['nombre'];
                $item->descripcion    = $row['descripcion'];
                $item->precio  = $row['precio'];
                $item->activo  = $row['activo'];
                $item->categoria_id  = $row['categoria_id'];
            }
            return $item;
        }catch(PDOException $e){
            return null;
        }
    }

    public function insert($datos){ // insertar

        $query = $this->db->connect()
            ->prepare('INSERT INTO productos (nombre, descripcion, precio, categoria_id, activo) 
                                VALUES(:nombre, :descripcion, :precio, :categoria_id, :activo)');
        try{
            $query->execute([
                'nombre' => $datos['nombre'],
                'descripcion' => $datos['descripcion'],
                'precio' => $datos['precio'],
                'categoria_id' => $datos['categoria_id'],
                'activo' => $datos['activo']
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }

    public function update($item){

        $query = $this->db->connect()
            ->prepare('UPDATE productos SET 
                      nombre = :nombre, 
                      descripcion = :descripcion,
                      precio = :precio,
                      categoria_id = :categoria_id, 
                      activo = :activo                     
                      WHERE id = :id');
        try{
            $query->execute([
                'id' => $item['id'],
                'nombre' => $item['nombre'],
                'descripcion' => $item['descripcion'],
                'precio' => $item['precio'],
                'categoria_id' => $item['categoria_id'],
                'activo' => $item['activo']
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    public function delete($id){
        $query = $this->db->connect()->prepare('DELETE FROM productos WHERE id = :id');
        try{
            $query->execute([
                'id' => $id
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
}
?>
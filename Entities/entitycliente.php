<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 12:37 PM
 */

class EntityClientes{
    public $id;
    public $nombre;
    public $direccion;
    public $telefono;
    public $nacionalidad;
    public $edad;
    public $email;
}
?>
<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 12:37 PM
 */

class EntityProducto{
    public $id;
    public $nombre;
    public $descripcion;
    public $precio;
    public $activo;
    public $categoria_id;
    public $categoria_nombre;

}
?>
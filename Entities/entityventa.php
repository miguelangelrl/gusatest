<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 12:37 PM
 */

class EntityVenta{
    public $id;
    public $titulo;
    public $clientes_id;
    public $clientes_nombre;
    public $detalles;
    public $total;
    public $asesores_id;
    public $asesores_nombre;
    public $comentarios;
    public $fecha_creacion;
    public $fecha_actualizacion;
    public $estatus;
}
?>
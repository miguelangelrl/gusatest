<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 12:37 PM
 */

class EntityAsesor{
    public $num_colaborador;
    public $nombre;
    public $direccion;
    public $telefono;
    public $email;
    public $activo;
}
?>
<?php
/**
 * Created by:
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 12:42 AM
 */
require 'libs/database.php';
require 'libs/model.php';
require 'libs/controller.php';
require 'libs/view.php';
require 'libs/app.php';
require 'config/config.php';
$app = new App();

?>
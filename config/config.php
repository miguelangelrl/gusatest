<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 11:37 AM
 */

define('URL', 'http://'.$_SERVER['SERVER_NAME'].'/gusatest/');

define ('HOST', 'localhost');
define ('DB', 'gusadb');
define ('USER', 'root');
define ('PASSWORD', '');
define ('CHARSET', 'utf8mb4');

?>
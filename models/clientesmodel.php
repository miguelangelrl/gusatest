<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 12:30 PM
 */

require 'Entities/entitycliente.php';

class ClientesModel extends Model{
    public function __construct(){
        parent::__construct();
    }
    public function get(){
        $items = [];
        try{
            $query = $this->db->connect()->query('SELECT * FROM clientes order by id DESC');

            while($row = $query->fetch()){
                $item = new EntityClientes();
                $item->id = $row['id'];
                $item->nombre = $row['nombre'];
                $item->direccion    = $row['direccion'];
                $item->telefono  = $row['telefono'];
                $item->nacionalidad  = $row['nacionalidad'];
                $item->edad  = $row['edad'];
                $item->email  = $row['email'];

                array_push($items, $item);
            }
            return $items;
        }catch(PDOException $e){
            return [];
        }
    }
    public function getById($id){
        $item = new EntityClientes();
        try{
            $query = $this->db->connect()->prepare('SELECT * FROM clientes WHERE id = :id');
            $query->execute(['id' => $id]);

            while($row = $query->fetch()){

                $item->id = $row['id'];
                $item->nombre = $row['nombre'];
                $item->direccion    = $row['direccion'];
                $item->telefono  = $row['telefono'];
                $item->nacionalidad  = $row['nacionalidad'];
                $item->edad  = $row['edad'];
                $item->email  = $row['email'];
            }
            return $item;
        }catch(PDOException $e){
            return null;
        }
    }

    public function insert($datos){
        // insertar


        $query = $this->db->connect()
            ->prepare('INSERT INTO clientes (nombre, direccion, telefono, nacionalidad, edad, email) 
                                VALUES(:nombre, :direccion, :telefono, :nacionalidad, :edad, :email)');
        try{
            $query->execute([
                'nombre' => $datos['nombre'],
                'direccion' => $datos['direccion'],
                'telefono' => $datos['telefono'],
                'nacionalidad' => $datos['nacionalidad'],
                'edad' => $datos['edad'],
                'email' => $datos['email']
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }


    }

    public function update($item){
//                echo '<pre>';
//        print_r($item['email']);
//        exit;
        $query = $this->db->connect()
            ->prepare('UPDATE clientes SET 
                      nombre = :nombre, 
                      direccion = :direccion,
                      telefono = :telefono,
                      nacionalidad = :nacionalidad, 
                      edad = :edad,
                      email = :email
                      WHERE id = :id');
        try{
            $query->execute([
                'id' => $item['id'],
                'nombre' => $item['nombre'],
                'direccion' => $item['direccion'],
                'telefono' => $item['telefono'],
                'nacionalidad' => $item['nacionalidad'],
                'edad' => $item['edad'],
                'email' => $item['email']
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
    public function delete($id){
        $query = $this->db->connect()->prepare('DELETE FROM clientes WHERE id = :id');
        try{
            $query->execute([
                'id' => $id
            ]);
            return true;
        }catch(PDOException $e){
            return false;
        }
    }
}
?>
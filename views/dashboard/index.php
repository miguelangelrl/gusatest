<?php require 'views/header.php'; ?>
<div class="container space">
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-1">
            <h1 class="text-center">Dashboard</h1>
            <br>
            <div class="row text-center">
                <a href="<?php echo constant('URL');?>ventas">
                    <div class="col-xs-6 col-sm-3">
                        <img src="<?php echo constant('URL');?>public/img/ventas.png" class="img-responsive"  width="200" height="200">
                        <br>
                        <label style="font-size: 1.5rem;">Ventas Totales:</label>&nbsp; <span class="badge" style="background-color: #3c8dbc; font-size: 1.5rem; font-weight: normal"><?php echo count($this->data['ventas']); ?></span>
                    </div>
                </a>
                <a href="<?php echo constant('URL');?>clientes">
                    <div class="col-xs-6 col-sm-3">
                        <img src="<?php echo constant('URL');?>public/img/clientes.png" class="img-responsive"  width="200" height="200">
                        <br>
                        <label style="font-size: 1.5rem;">Clientes Totales:</label>&nbsp; <span class="badge" style="background-color: #3c8dbc; font-size: 1.5rem; font-weight: normal"><?php echo count($this->data['clientes']); ?></span>
                        <hr>
                    </div>
                </a>
                <a href="<?php echo constant('URL');?>productos">
                    <div class="col-xs-6 col-sm-3">
                        <img src="<?php echo constant('URL');?>public/img/productos.png" class="img-responsive" width="200" height="200">
                        <br>
                        <label style="font-size: 1.5rem;">Productos Totales:</label>&nbsp; <span class="badge" style="background-color: #3c8dbc; font-size: 1.5rem; font-weight: normal"><?php echo count($this->data['productos']); ?></span>
                    </div>
                </a>
                <a href="<?php echo constant('URL');?>asesores">
                    <div class="col-xs-6 col-sm-3">
                        <img src="<?php echo constant('URL');?>public/img/asesores.png" class="img-responsive" width="200" height="200">
                        <br>
                        <label style="font-size: 1.5rem;">Asesores Totales:</label>&nbsp; <span class="badge" style="background-color: #3c8dbc; font-size: 1.5rem; font-weight: normal""><?php echo count($this->data['asesores']); ?></span>
                    </div>
                </a>
            </div>
            <br>
            <h2 class="text-center">Ultimas 10 Ventas</h2>
            <div class="table-responsive">
                <table class="table table-striped text-center">
                    <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Titulo</th>
                        <th class="text-center">Cliente</th>
                        <th class="text-center">Total</th>
                        <th class="text-center">Asesor</th>
                        <th class="text-center">Comentarios</th>
                        <th class="text-center">Fecha Alta</th>
                        <th class="text-center">Fecha Modificación</th>
                        <th class="text-center">Status</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php
                    include_once 'Entities/entityventa.php';
                    if(isset($this->data['ventas'])){
                        foreach ($this->data['ventas'] as $row) {
                            $venta = new EntityVenta();
                            $venta = $row;
                            ?>
                            <tr>
                                <td><?php echo $venta->id; ?></td>
                                <td><?php echo $venta->titulo; ?></td>
                                <td><?php echo $venta->clientes_nombre; ?></td>
                                <td><?php echo $venta->total; ?></td>
                                <td><?php echo $venta->asesores_nombre?></td>
                                <td><?php echo $venta->comentarios; ?></td>
                                <td><?php echo date('Y-m-d', strtotime($venta->fecha_creacion)); ?></td>
                                <td><?php echo date('Y-m-d', strtotime($venta->fecha_actualizacion)); ?></td>
                                <td><?php echo $venta->estatus; ?></td>
                            </tr>
                        <?php
                        }
                    }else{
                        echo "<tr><td>Sin Ventas</td></tr>";
                    }
                    ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php require 'views/footer.php'; ?>


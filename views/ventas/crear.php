<?php require 'views/header.php';?>
<div class="container space">
    <h2 class="text-center">Nueva venta</h2>
    <hr>
    <div class="row">
        <form class="form-horizontal" action="<?php echo constant('URL'); ?>ventas/guardarVenta" method="POST">
            <div class="col-md-4">
                <a class="btn btn-back pull-right" href="<?php echo constant('URL');?>ventas">Regresar</a>
                <div class="form-group">
                    <input class="btn btn-success" type="submit" value="Guardar Venta">
                </div>
                <hr>
                <br>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="">Título</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="titulo" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="">Cliente</label>
                    <div class="col-sm-9">
                        <select name="clientes_id" id="" class="form-control" required>
                            <?php
                            foreach ($this->data['clientes'] as $cliente){
                                echo "<option value='$cliente->id'>$cliente->nombre</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="">Asesor</label>
                    <div class="col-sm-9">
                        <select name="asesores_id" id="" class="form-control" required>
                            <?php
                            foreach ($this->data['asesores'] as $asesor){
                                echo "<option value='$asesor->num_colaborador'>$asesor->nombre</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="">Comentarios</label>
                    <div class="col-sm-9">
                        <input type="text" name="comentarios" id="" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="">Estatus</label>
                    <div class="col-sm-9">
                        <select name="estatus" id="" class="form-control" required>
                            <option value="">Selecciona...</option>
                            <option value="Vendida">Vendida</option>
                            <option value="Pendiente">Pendiente</option>
                            <option value="Cancelada">Cancelada</option>
                        </select>
                    </div>
                </div>
                <div class="contorno">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="">Producto</label>
                        <div class="col-sm-9">
                            <select name="producto_id" id="producto_id" class="form-control" required>
                                <?php
                                foreach ($this->data['productos'] as $producto){
                                    echo "<option value='$producto->id'>$producto->nombre</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="">Cantidad</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="number" name="producto_cantidad" id="producto_cantidad" value="1" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input class="btn btn-back form-control pull-right" type="button" value="Agregar" id="agregar_producto">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-sm-3 col-xs-12 control-label text-uppercase resumen" for="">Resumen</label>
                    <label class="col-sm-2 col-xs-6 control-label text-uppercase resumen" for="">Total:<span class="glyphicon glyphicon-usd"></span></label>
                    <div class="col-sm-3 col-xs-6">
                         <input style="font-size: 20px" type="text" readonly value="" name="total_venta" class="form-control">
                    </div>
                    <div id="agregar_producto" class="table-responsive col-md-12">
                        <table class="table table-striped text-center">
                            <thead>
                                <tr>
                                    <th class="text-center">Id Producto</th>
                                    <th class="text-center">Producto</th>
                                    <th class="text-center">Descripción</th>
                                    <th class="text-center">Precio Unitario</th>
                                    <th class="text-center">Cantidad</th>
                                    <th class="text-center">Subtotal</th>
                                </tr>
                            </thead>

                            <tbody name="mas_producto">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php require 'views/footer.php';?>

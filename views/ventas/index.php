<?php require 'views/header.php'; ?>
<div class="container space">
    <h2 class="text-center">Listado de Ventas</h2>
    <div class="table-responsive">
        <a class="btn btn-success" href="<?php echo constant('URL');?>ventas/crearVenta">Nueva Venta</a>
        <table class="table table-striped text-center">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Titulo</th>
                    <th class="text-center">Cliente</th>
                    <th class="text-center">Total</th>
                    <th class="text-center">Asesor</th>
                    <th class="text-center">Comentarios</th>
                    <th class="text-center">Fecha Alta</th>
                    <th class="text-center">Fecha Modificación</th>
                    <th class="text-center">Status</th>
                    <th class="text-center" colspan="2">Opciones</th>
                </tr>
            </thead>

            <tbody>
                <?php
                include_once 'Entities/entityventa.php';
                foreach ($this->ventas as $row) {
                    $venta = new EntityVenta();
                    $venta = $row;
                    ?>
                    <tr>
                        <td><?php echo $venta->id; ?></td>
                        <td><?php echo $venta->titulo; ?></td>
                        <td><?php echo $venta->clientes_nombre; ?></td>
                        <td><?php echo $venta->total; ?></td>
                        <td><?php echo $venta->asesores_nombre?></td>
                        <td><?php echo $venta->comentarios; ?></td>
                        <td><?php echo date('Y-m-d', strtotime($venta->fecha_creacion)); ?></td>
                        <td><?php echo date('Y-m-d', strtotime($venta->fecha_actualizacion)); ?></td>
                        <td><?php echo $venta->estatus; ?></td>
                        <td><a class="btn btn-warning" href="<?php echo constant('URL') . 'ventas/verVenta/' . $venta->id; ?>">Actualizar</a></td>
                        <td><a class="btn btn-danger" href="<?php echo constant('URL') . 'ventas/eliminarVenta/' . $venta->id; ?>">Eliminar</a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php require 'views/footer.php'; ?>

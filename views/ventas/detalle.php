<?php require 'views/header.php'; ?>
<div class="container space">
    <h2 class="text-center">Detalles Venta ID: <?php echo $this->data['venta']->id; ?></h2>
    <hr>
    <div class="row">
        <form class="form-horizontal" action="<?php echo constant('URL'); ?>ventas/actualizarVenta" method="POST">
            <input name="venta_id" type="hidden" value="<?php echo $this->data['venta']->id; ?>">
            <div class="col-md-4">
                <a class="btn btn-back pull-right" href="<?php echo constant('URL');?>ventas">Regresar</a>
                <div class="form-group">
                    <input class="btn btn-success" type="submit" value="Actualizar Venta">
                </div>
                <hr>
                <br>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="">Título</label>
                    <div class="col-sm-9">
                        <input class="form-control" type="text" name="titulo" value="<?php echo $this->data['venta']->titulo; ?>" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="">Cliente</label>
                    <div class="col-sm-9">
                        <select name="clientes_id" id="" class="form-control" required>
                            <?php
                            foreach ($this->data['clientes'] as $cliente){
                                echo "<option value='$cliente->id'";if($cliente->id==$this->data['venta']->clientes_id)echo'selected';echo">$cliente->nombre</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="">Asesor</label>
                    <div class="col-sm-9">
                        <select name="asesores_id" id="" class="form-control" required>
                            <?php
                            foreach ($this->data['asesores'] as $asesor){
                                echo "<option value='$asesor->num_colaborador'";if($asesor->num_colaborador==$this->data['venta']->asesores_id)echo'selected';echo">$asesor->nombre</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="">Fecha Alta</label>
                    <div class="col-sm-9">
                        <input type="date" name="fecha_creacion" class="form-control" value="<?php echo date('Y-m-d', strtotime($this->data['venta']->fecha_creacion)); ?>" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="">Comentarios</label>
                    <div class="col-sm-9">
                        <input type="text" name="comentarios" id="" class="form-control" value="<?php echo $this->data['venta']->comentarios; ?>"required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="">Estatus</label>
                    <div class="col-sm-9">
                        <select name="estatus" id="" class="form-control" required>
                            <option value="">Selecciona...</option>
                            <option value="Vendida"<?php if($this->data['venta']->estatus=='Vendida')echo 'selected'; ?>>Vendida</option>
                            <option value="Pendiente" <?php if($this->data['venta']->estatus=='Pendiente')echo 'selected'; ?>>Pendiente</option>
                            <option value="Cancelada" <?php if($this->data['venta']->estatus=='Cancelada')echo 'selected'; ?>>Cancelada</option>
                        </select>
                    </div>
                </div>
                <div class="contorno">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="">Producto</label>
                        <div class="col-sm-9">
                            <select name="producto_id" id="producto_id" class="form-control" required>
                                <?php
                                foreach ($this->data['productos'] as $producto){
                                    echo "<option value='$producto->id'>$producto->nombre</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="">Cantidad</label>
                        <div class="col-sm-9">
                            <input class="form-control" type="number" name="producto_cantidad" id="producto_cantidad" value="1" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input class="btn btn-back form-control pull-right" type="button" value="Agregar" id="agregar_producto" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-sm-3 col-xs-12 control-label text-uppercase resumen" for="">Resumen</label>
                    <label class="col-sm-2 col-xs-6 control-label text-uppercase resumen" for="">Total:<span class="glyphicon glyphicon-usd"></span></label>
                    <div class="col-sm-3 col-xs-6">
                        <input style="font-size: 20px" type="text" readonly value="" name="total_venta" class="form-control">
                    </div>
                    <div id="agregar_producto" class="table-responsive col-md-12">

                        <table class="table table-striped text-center">
                            <thead>
                            <tr>
                                <th class="text-center">Id Producto</th>
                                <th class="text-center">Producto</th>
                                <th class="text-center">Descripción</th>
                                <th class="text-center">Precio Unitario</th>
                                <th class="text-center">Cantidad</th>
                                <th class="text-center">Subtotal</th>
                            </tr>
                            </thead>

                            <tbody name="mas_producto">
                            <?php
                            foreach ($this->data['venta']->detalles as $producto){

                                echo  '<tr>
                                            <td>'.$producto->products_id.'</td>
                                            <td>'.$producto->products_nombre.'</td>
                                            <td>'.$producto->products_descripcion.'</td>
                                            <td>'.$producto->products_precio.'</td>
                                            <td>'.$producto->products_cantidad.'</td>
                                            <td name="subtotal[]">'.$producto->products_subtotal.'</td>
                                            <td><input type="button" class="btn btn-warning borrar_producto"" value="X"></td>
                                            
                                            <input name="products_id[]" type="hidden" value="'.$producto->products_id.'">
                                            <input name="products_nombre[]" type="hidden" value="'.$producto->products_nombre.'">
                                            <input name="products_descripcion[]" type="hidden" value="'.$producto->products_descripcion.'">
                                            <input name="products_precio[]" type="hidden" value="'.$producto->products_precio.'">
                                            <input name="products_cantidad[]" type="hidden" value="'.$producto->products_cantidad.'">
                                            <input name="products_subtotal[]" type="hidden" value="'.$producto->products_subtotal.'">
                                        </tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php require 'views/footer.php'; ?>

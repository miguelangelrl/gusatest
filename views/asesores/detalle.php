<?php require 'views/header.php'; ?>

<div class="container space">
    <div class="col-md-6 col-md-offset-3">
        <h2 class="center">Actualizar Asesor ID: <?php echo $this->asesor->num_colaborador; ?></h2>
        <a class="btn btn-back pull-right" href="<?php echo constant('URL');?>asesores">Regresar</a>
        <hr>
        <br>
        <form class="form-horizontal" action="<?php echo constant('URL'); ?>asesores/actualizarAsesor/" method="POST">

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Num. Colaborador</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="num_colaborador" readonly="" value="<?php echo $this->asesor->num_colaborador; ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Nombre</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="nombre" value="<?php echo $this->asesor->nombre; ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Direccion</label>
                <div class="col-sm-9">
                    <input type="text" name="direccion" id="" class="form-control" value="<?php echo $this->asesor->direccion; ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Teléfono</label>
                <div class="col-sm-9">
                    <input type="text" name="telefono" id="" class="form-control" value="<?php echo $this->asesor->telefono; ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Email</label>
                <div class="col-sm-9">
                    <input type="email" name="email" id="" class="form-control" value="<?php echo $this->asesor->email; ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Activo</label>
                <div class="col-sm-9">
                    <select name="activo" id="" class="form-control" required>
                        <option value="">Selecciona...</option>
                        <option value="1" <?php if($this->asesor->activo==1)echo 'selected';?>>Activo</option>
                        <option value="0" <?php if($this->asesor->activo==0)echo 'selected';?>>Inactivo</option>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <input class="btn btn-success" type="submit" value="Actualizar">
            </div>
        </form>
    </div>
</div>
<?php require 'views/footer.php'; ?>

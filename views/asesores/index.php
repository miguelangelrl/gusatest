<?php require 'views/header.php'; ?>
<div class="container space">
    <h2 class="text-center">Listado de Asesores</h2>
    <div class="table-responsive">
        <a class="btn btn-success" href="<?php echo constant('URL');?>asesores/crearAsesor">Nuevo Asesor</a>
        <table class="table table-striped text-center">
            <thead>
                <tr>
                    <th class="text-center">Num. Colaborador</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Dirección</th>
                    <th class="text-center">Teléfono</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Activo</th>
                    <th class="text-center" colspan="2">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                include_once 'Entities/entityasesor.php';
                foreach ($this->asesores as $row) {
                    $asesor = new EntityAsesor();
                    $asesor = $row;
                    ?>
                    <tr>
                        <td><?php echo $asesor->num_colaborador; ?></td>
                        <td><?php echo $asesor->nombre; ?></td>
                        <td><?php echo $asesor->direccion; ?></td>
                        <td><?php echo $asesor->telefono; ?></td>
                        <td><?php echo $asesor->email; ?></td>
                        <td><?php echo ($asesor->activo == 1)? 'Activo': 'Inactivo'?></td>
                        <td><a class="btn btn-warning" href="<?php echo constant('URL') . 'asesores/verAsesor/' . $asesor->num_colaborador; ?>">Actualizar</a></td>
                        <td><a class="btn btn-danger" href="<?php echo constant('URL') . 'asesores/eliminarAsesor/' . $asesor->num_colaborador; ?>">Eliminar</a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php require 'views/footer.php'; ?>

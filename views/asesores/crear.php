<?php require 'views/header.php';?>

<div class="container space">

    <div class="col-md-6 col-md-offset-3">
        <h2 class="text-center">Nuevo Asesor</h2>
        <a class="btn btn-back pull-right" href="<?php echo constant('URL');?>asesores">Regresar</a>
        <hr>
        <br>
        <form class="form-horizontal" action="<?php echo constant('URL'); ?>asesores/guardarAsesor" method="POST">

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Num. Colaborador</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="num_colaborador" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Nombre</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="nombre" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Direccion</label>
                <div class="col-sm-9">
                    <input type="text" name="direccion" id="" class="form-control" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Teléfono</label>
                <div class="col-sm-9">
                    <input type="text" name="telefono" id="" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Email</label>
                <div class="col-sm-9">
                    <input type="email" name="email" id="" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Activo</label>
                <div class="col-sm-9">
                    <select name="activo" id="" class="form-control" required>
                        <option value="1">Activo</option>
                        <option value="0">Inactivo</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <input class="btn btn-success" type="submit" value="Guardar">
            </div>
        </form>
    </div>
</div>
<?php require 'views/footer.php';?>

<?php require 'views/header.php'; ?>

<div class="container space">
    <div class="col-md-6 col-md-offset-3">
        <h2 class="text-center">Actualizar Cliente ID: <?php echo $this->cliente->id; ?></h2>
        <a class="btn btn-back pull-right" href="<?php echo constant('URL');?>clientes">Regresar</a>
        <hr>
        <br>
        <form class="form-horizontal" action="<?php echo constant('URL'); ?>clientes/actualizarCliente/" method="POST">


            <div class="form-group">
                <label class="col-sm-3 control-label" for="">ID</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="id" value="<?php echo $this->cliente->id; ?>"readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Nombre</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="nombre" value="<?php echo $this->cliente->nombre; ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Direccion</label>
                <div class="col-sm-9">
                    <input type="text" name="direccion" id="" class="form-control" value="<?php echo $this->cliente->direccion; ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Teléfono</label>
                <div class="col-sm-9">
                    <input type="text" name="telefono" id="" class="form-control" value="<?php echo $this->cliente->telefono; ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"  for="">Nacionalidad</label>
                <div class="col-sm-9">
                    <input type="text" name="nacionalidad" id="" class="form-control" value="<?php echo $this->cliente->nacionalidad; ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Edad</label>
                <div class="col-sm-9">
                    <input type="number" name="edad" id="" class="form-control" value="<?php echo $this->cliente->edad; ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Email</label>
                <div class="col-sm-9">
                    <input type="email" name="email" id="" class="form-control" value="<?php echo $this->cliente->email; ?>" required>
                </div>
            </div>
            <div class="form-group">
                <input class="btn btn-success" type="submit" value="Actualizar">
            </div>
        </form>
    </div>
</div>
<?php require 'views/footer.php'; ?>

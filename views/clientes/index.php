<?php require 'views/header.php'; ?>
<div class="container space">
    <h2 class="text-center">Listado de Clientes</h2>
    <div class="table-responsive">
        <a class="btn btn-success" href="<?php echo constant('URL');?>clientes/crearCliente">Nuevo Cliente</a>
        <table class="table table-striped text-center">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Dirección</th>
                    <th class="text-center">Teléfono</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Nacionalidad
                    <th class="text-center">Edad</th>
                    <th class="text-center" colspan="2">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                include_once 'Entities/entitycliente.php';
                foreach ($this->clientes as $row) {
                    $cliente = new EntityClientes();
                    $cliente = $row;
                    ?>
                    <tr>
                        <td><?php echo $cliente->id; ?></td>
                        <td><?php echo $cliente->nombre; ?></td>
                        <td><?php echo $cliente->direccion; ?></td>
                        <td><?php echo $cliente->telefono; ?></td>
                        <td><?php echo $cliente->email; ?></td>
                        <td><?php echo $cliente->nacionalidad; ?></td>
                        <td><?php echo $cliente->edad; ?></td>
                        <td><a class="btn btn-warning" href="<?php echo constant('URL') . 'clientes/verCliente/' . $cliente->id; ?>">Actualizar</a></td>
                        <td><a class="btn btn-danger" href="<?php echo constant('URL') . 'clientes/eliminarCliente/' . $cliente->id; ?>">Eliminar</a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php require 'views/footer.php'; ?>

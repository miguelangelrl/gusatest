<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title id="titulo">Document</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo constant('URL');?>public/img/favicon.png">
    <link rel="stylesheet" href="<?php echo constant('URL');?>public/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo constant('URL');?>public/css/default.css">
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo constant('URL');?>dashboard"><img alt="Brand" src="<?php echo constant('URL');?>public/img/SampleLogo.png" class="img-responsive"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo constant('URL');?>dashboard">Dashboard</a></li>
                    <li><a href="<?php echo constant('URL');?>clientes">Clientes</a></li>
                    <li><a href="<?php echo constant('URL');?>asesores">Asesores</a></li>
                    <li><a href="<?php echo constant('URL');?>ventas">Ventas</a></li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Más<span class="glyphicon glyphicon-menu-down"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo constant('URL');?>productos">Productos</a></li>
                            <li><a href="<?php echo constant('URL');?>categorias">Categorías Productos</a></li>
                        </ul>
                    </li>

                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>
    <?php
    if(!empty($this->mensaje)){
        echo "<div class=\"alert alert-success alert-dismissible fade in\" role=\"alert\" id=\"mensaje\">                            
                <strong>$this->mensaje</strong>
              </div>
        ";
    }
    ?>



<?php require 'views/header.php'; ?>

<div class="container space">
    <div class="col-md-6 col-md-offset-3">
        <h2 class="text-center">Actualizar Producto: <?php echo $this->data['producto']->nombre; ?></h2>
        <a class="btn btn-back pull-right" href="<?php echo constant('URL');?>productos">Regresar</a>
        <hr>
        <br>
        <form class="form-horizontal" action="<?php echo constant('URL'); ?>productos/actualizarProducto/" method="POST">


            <div class="form-group">
                <label class="col-sm-3 control-label" for="">ID</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="id" value="<?php echo $this->data['producto']->id; ?>"readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Nombre</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="nombre" value="<?php echo $this->data['producto']->nombre; ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Descripción</label>
                <div class="col-sm-9">
                    <textarea name="descripcion" id=" cols="30" rows="10" class="form-control" required><?php echo $this->data['producto']->descripcion; ?></textarea>

                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Precio</label>
                <div class="col-sm-9">
                    <input type="text" name="precio" id="" class="form-control" value="<?php echo $this->data['producto']->precio; ?>" required>
                </div>
            </div>



            <div class="form-group">
                <label class="col-sm-3 control-label"  for="">Categoría</label>
                <div class="col-sm-9">
                    <select name="categoria_id" id="" class="form-control" required>
                        <?php
                            foreach ($this->data['categorias']as $categoria){
                                echo "<option value='$categoria->id'";if($this->data['producto']->categoria_id==$categoria->id)echo'selected';echo">$categoria->nombre</option>";
                            }
                        ?>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Activo</label>
                <div class="col-sm-9">
                    <select name="activo" id="" class="form-control" required>
                        <option value="">Selecciona...</option>
                        <option value="1" <?php if($this->data['producto']->activo==1)echo 'selected';?>>Activo</option>
                        <option value="0" <?php if($this->data['producto']->activo==0)echo 'selected';?>>Inactivo</option>
                    </select>

                </div>
            </div>


            <div class="form-group">
                <input class="btn btn-success" type="submit" value="Actualizar">
            </div>
        </form>
    </div>
</div>
<?php require 'views/footer.php'; ?>

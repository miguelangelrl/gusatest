<?php require 'views/header.php'; ?>
<div class="container space">
    <h2 class="text-center">Listado de Productos</h2>

    <div class="table-responsive">
        <a class="btn btn-success" href="<?php echo constant('URL');?>productos/crearProducto">Nuevo Producto</a>
        <table class="table table-striped text-center">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Descripción</th>
                    <th class="text-center">Precio</th>
                    <th class="text-center">Activo</th>
                    <th class="text-center">Categoría
                    <th class="text-center" colspan="2">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                include_once 'Entities/entityproducto.php';
                foreach ($this->productos as $row) {
                    $producto = new EntityProducto();
                    $producto = $row;
                    ?>
                    <tr>
                        <td><?php echo $producto->id; ?></td>
                        <td><?php echo $producto->nombre; ?></td>
                        <td class="truncar-texto"><?php echo $producto->descripcion; ?></td>
                        <td><?php echo $producto->precio; ?></td>
                        <td><?php echo ($producto->activo == 1)? 'Activo': 'Inactivo'?></td>
                        <td><?php echo $producto->categoria_nombre; ?></td>
                        <td><a class="btn btn-warning" href="<?php echo constant('URL') . 'productos/verProducto/' . $producto->id; ?>">Actualizar</a></td>
                        <td><a class="btn btn-danger" href="<?php echo constant('URL') . 'productos/eliminarProducto/' . $producto->id; ?>">Eliminar</a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php require 'views/footer.php'; ?>

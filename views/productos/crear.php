<?php require 'views/header.php';?>

<div class="container space">

    <div class="col-md-6 col-md-offset-3">
        <h2 class="text-center">Nuevo Producto</h2>
        <a class="btn btn-back pull-right" href="<?php echo constant('URL');?>productos">Regresar</a>
        <hr>
        <br>
        <form class="form-horizontal" action="<?php echo constant('URL'); ?>productos/guardarProducto" method="POST">

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Nombre</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="nombre" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Descripción</label>
                <div class="col-sm-9">
                    <textarea name="descripcion" id="" cols="30" rows="10" class="form-control"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Precio</label>
                <div class="col-sm-9">
                    <input type="number" name="precio" id="" class="form-control" required>
                </div>
            </div>



            <div class="form-group">
                <label class="col-sm-3 control-label"  for="">Categoría</label>
                <div class="col-sm-9">
                    <select name="categoria_id" id="" class="form-control">
                        <?php
                        foreach ($this->categorias as $categoria){
                            echo "<option value='$categoria->id'>$categoria->nombre</option>";
                        }
                        ?>
                    </select>

                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Activo</label>
                <div class="col-sm-9">
                    <select name="activo" id="" class="form-control">
                        <option value="">Selecciona...</option>
                        <option value="1">Activo</option>
                        <option value="0">Inactivo</option>
                    </select>

                </div>
            </div>
            <div class="form-group">
                <input class="btn btn-success" type="submit" value="Guardar">
            </div>
        </form>
    </div>
</div>
<?php require 'views/footer.php';?>

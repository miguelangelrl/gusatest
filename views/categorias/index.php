<?php require 'views/header.php'; ?>
<div class="container space">
    <h2 class="text-center">Listado de Categorías</h2>
    <div class="table-responsive">
        <a class="btn btn-success" href="<?php echo constant('URL');?>categorias/crearCategoria">Nueva Categoría</a>
        <table class="table table-striped text-center">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th class="text-center">Nombre</th>
                    <th class="text-center">Descripción</th>
                    <th class="text-center" colspan="2">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                include_once 'Entities/entitycategoria.php';
                foreach ($this->categorias as $row) {
                    $categoria = new EntityCategoria();
                    $categoria = $row;
                    ?>
                    <tr>
                        <td><?php echo $categoria->id; ?></td>
                        <td><?php echo $categoria->nombre; ?></td>
                        <td><?php echo $categoria->descripcion; ?></td>
                        <td><a class="btn btn-warning" href="<?php echo constant('URL') . 'categorias/verCategoria/' . $categoria->id; ?>">Actualizar</a></td>
                        <td><a class="btn btn-danger" href="<?php echo constant('URL') . 'categorias/eliminarCategoria/' . $categoria->id; ?>">Eliminar</a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php require 'views/footer.php'; ?>

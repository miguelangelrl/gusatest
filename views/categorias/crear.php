<?php require 'views/header.php';?>
<div class="container space">
    <div class="col-md-6 col-md-offset-3">
        <h2 class="text-center">Nueva Categoría</h2>
        <a class="btn btn-back pull-right" href="<?php echo constant('URL');?>categorias">Regresar</a>
        <hr>
        <br>
        <form class="form-horizontal" action="<?php echo constant('URL'); ?>categorias/guardarCategoria" method="POST">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Nombre</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="nombre" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="">Descripción</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="descripcion" id="" cols="30" rows="10" required></textarea>

                </div>
            </div>

            <div class="form-group">
                <input class="btn btn-success" type="submit" value="Guardar">
            </div>
        </form>
    </div>
</div>
<?php require 'views/footer.php';?>

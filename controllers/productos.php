<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 10:55 AM
 */

require_once 'models/categoriasmodel.php';
class Productos extends controller
{
    function __construct(){
        parent::__construct();
        $this->view->mensaje = "";
    }
    function render(){
        $productos = $this->model->get();

        $this->view->productos = $productos;
        $this->view->render('productos/index');
    }


    function verProducto($param = null){
        $id = $param[0];
        $producto = $this->model->getById($id);

        $c = new CategoriasModel();
        $categorias = $c->get();

        $this->view->data = array('producto'=>$producto, 'categorias'=>$categorias);
        $this->view->render('productos/detalle');
    }

    function verProductoAjax($param = null){
        $html = '';

        $id = $param[0];
        $producto = $this->model->getById($id);

        $html = '<tr>
                    <td>'.$producto->id.'</td>
                    <td>'.$producto->nombre.'</td>
                    <td>'.$producto->descripcion.'</td>
                    <td>'.$producto->precio.'</td>
                    <td>'.$param[1].'</td>
                    <td name="subtotal[]">'.$producto->precio*$param[1].'</td>
                    <td><input type="button" class="btn btn-warning borrar_producto"" value="X"></td>
                    
                    <input name="products_id[]" type="hidden" value="'.$producto->id.'">
                    <input name="products_nombre[]" type="hidden" value="'.$producto->nombre.'">
                    <input name="products_descripcion[]" type="hidden" value="'.$producto->descripcion.'">
                    <input name="products_precio[]" type="hidden" value="'.$producto->precio.'">
                    <input name="products_cantidad[]" type="hidden" value="'.$param[1].'">
                    <input name="products_subtotal[]" type="hidden" value="'.$producto->precio*$param[1].'">
                </tr>';

        echo $html;
    }

    function crearProducto(){
        $c = new CategoriasModel();
        $categorias = $c->get();

        $this->view->categorias = $categorias;
        
        $this->view->render('productos/crear');
    }
    function guardarProducto(){
        $nombre    = $_POST['nombre'];
        $descripcion  = $_POST['descripcion'];
        $precio  = $_POST['precio'];
        $categoria_id  = $_POST['categoria_id'];
        $activo  = $_POST['activo'];

        if($this->model->insert([
            'nombre' => $nombre,
            'descripcion' => $descripcion,
            'precio' => $precio,
            'categoria_id' => $categoria_id,
            'activo' => $activo
        ]))
        {
            $this->view->mensaje = "Producto agregado correctamente";

            $this->render();
        }else{
            $this->view->mensaje = "El Producto ya existe";
            $this->view->render('productos/crear');
        }
    }
    function actualizarProducto($param = null){

        $id = $_POST['id'];
        $nombre    = $_POST['nombre'];
        $descripcion  = $_POST['descripcion'];
        $precio  = $_POST['precio'];
        $categoria_id  = $_POST['categoria_id'];
        $activo  = $_POST['activo'];

        if($this->model->update([
            'id' => $id,
            'nombre' => $nombre,
            'descripcion' => $descripcion,
            'precio' => $precio,
            'categoria_id' => $categoria_id,
            'activo' => $activo]))
        {
            $producto = new EntityProducto();
            $producto->id = $id;
            $producto->nombre = $nombre;
            $producto->descripcion = $descripcion;
            $producto->precio = $precio;
            $producto->categoria_id = $categoria_id;
            $producto->activo = $activo;

            $c = new CategoriasModel();
            $categorias = $c->get();

            $this->view->data = array('producto'=>$producto, 'categorias'=>$categorias);

            $this->view->mensaje = "Producto actualizado correctamente";
        }else{
            $this->view->mensaje = "No se pudo actualizar el Producto";
        }
        $this->view->render('productos/detalle');
    }
    function eliminarProducto($param = null){
        $id = $param[0];
        if($this->model->delete($id)){
            $this->view->mensaje = "Producto eliminado correctamente";
        }else{
            $this->view->mensaje = "No se pudo eliminar el producto";
        }
        $this->render();
    }
}
?>
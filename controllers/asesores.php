<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 10:55 AM
 */
class Asesores extends controller
{
    function __construct(){
        parent::__construct();
        $this->view->mensaje = "";
    }
    function render(){
        $asesores = $this->view->datos = $this->model->get();

        $this->view->asesores = $asesores;
        $this->view->render('asesores/index');
    }
    function verAsesor($param = null){
        $id = $param[0];
        $asesor = $this->model->getById($id);
        session_start();
        $_SESSION["id"] = $asesor->id;
        $this->view->asesor = $asesor;
        $this->view->render('asesores/detalle');
    }

    function crearAsesor(){
        $this->view->render('asesores/crear');
    }
    function guardarAsesor(){
        $num_colaborador    = $_POST['num_colaborador'];
        $nombre    = $_POST['nombre'];
        $direccion  = $_POST['direccion'];
        $telefono  = $_POST['telefono'];
        $email  = $_POST['email'];
        $activo  = $_POST['activo'];
        if($this->model->insert([
            'num_colaborador' => $num_colaborador,
            'nombre' => $nombre,
            'direccion' => $direccion,
            'telefono' => $telefono,
            'email' => $email,
            'activo' => $activo

        ]))
        {
            $this->view->mensaje = "Asesor creado correctamente";

            $this->render();
        }else{
            $this->view->mensaje = "El asesor ya existe";
            $this->view->render('asesores/crear');
        }
    }
    function actualizarAsesor($param = null){
        $num_colaborador    = $_POST['num_colaborador'];
        $nombre    = $_POST['nombre'];
        $direccion  = $_POST['direccion'];
        $telefono  = $_POST['telefono'];
        $email  = $_POST['email'];
        $activo  = $_POST['activo'];

        if($this->model->update([
            'num_colaborador' => $num_colaborador,
            'nombre' => $nombre,
            'direccion' => $direccion,
            'telefono' => $telefono,
            'email' => $email,
            'activo' => $activo ]))
        {
            $asesor = new EntityAsesor();
            $asesor->num_colaborador = $num_colaborador;
            $asesor->nombre = $nombre;
            $asesor->direccion = $direccion;
            $asesor->telefono = $telefono;
            $asesor->email = $email;
            $asesor->activo = $activo;

            $this->view->asesor = $asesor;
            $this->view->mensaje = "Asesor actualizado correctamente";
        }else{
            $this->view->mensaje = "No se pudo actualizar al asesor";
        }
        $this->view->render('asesores/detalle');
    }
    function eliminarAsesor($param = null){
        $id = $param[0];
        if($this->model->delete($id)){
            $this->view->mensaje = "Asesor eliminado correctamente";
        }else{
            $this->view->mensaje = "No se pudo eliminar al asesor";
        }
        $this->render();
    }

}
?>
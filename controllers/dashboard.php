<?php
/**
 * Created by:
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 07:48 AM
 */
require_once 'models/clientesmodel.php';
require_once 'models/asesoresmodel.php';
require_once 'models/productosmodel.php';
require_once 'models/ventasmodel.php';

class dashboard extends controller {
    function __construct()
    {
        parent::__construct();

    }

    function render(){
        $limit = 10;
        $c = new ClientesModel();
        $clientes = $c->get();

        $a = new AsesoresModel();
        $asesores = $a ->get();

        $p = new ProductosModel();
        $productos = $p->get();

        $v = new VentasModel();
        $ventas = $v->getLimit($limit);

        $this->view->data = array('ventas'=>$ventas, 'clientes' => $clientes, 'asesores' => $asesores, 'productos' => $productos);

        $this->view->render('dashboard/index');
    }

}
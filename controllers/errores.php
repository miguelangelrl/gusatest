<?php
/**
 * Created by:
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 07:53 AM
 */
class errores extends controller
{
    function __construct()
    {
        parent::__construct();
        $this->view->mensaje = "Hubo un error en la solicitud o no existe la página";
        $this->view->render('errores/dashboard');

    }
}
?>
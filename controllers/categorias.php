<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 10:55 AM
 */
class Categorias extends controller
{
    function __construct(){
        parent::__construct();
        $this->view->mensaje = "";
    }
    function render(){
        $categorias = $this->view->datos = $this->model->get();

        $this->view->categorias = $categorias;
        $this->view->render('categorias/index');
    }
    function verCategoria($param = null){
        $id = $param[0];
        $categoria = $this->model->getById($id);
        session_start();
        $_SESSION["id"] = $categoria->id;
        $this->view->categoria = $categoria;
        $this->view->render('categorias/detalle');
    }

    function crearCategoria(){
        $this->view->render('categorias/crear');
    }
    function guardarCategoria(){
        $nombre    = $_POST['nombre'];
        $descripcion  = $_POST['descripcion'];

        if($this->model->insert([
            'nombre' => $nombre,
            'descripcion' => $descripcion

        ]))
        {
            $this->view->mensaje = "Categoría creada correctamente";

            $this->render();
        }else{
            $this->view->mensaje = "La categoría ya existe";
            $this->view->render('categorias/crear');
        }
    }
    function actualizarCategoria($param = null){
        $id    = $_POST['id'];
        $nombre    = $_POST['nombre'];
        $descripcion  = $_POST['descripcion'];

        if($this->model->update([
            'id' => $id,
            'nombre' => $nombre,
            'descripcion' => $descripcion ]))
        {
            $categoria = new EntityCategoria();

            $categoria->nombre = $nombre;
            $categoria->descripcion = $descripcion;


            $this->view->categoria = $categoria;
            $this->view->mensaje = "Categoría actualizada correctamente";
        }else{
            $this->view->mensaje = "No se pudo actualizar la categoría";
        }
        $this->view->render('categorias/detalle');
    }
    function eliminarCategoria($param = null){
        $id = $param[0];
        if($this->model->delete($id)){
            $this->view->mensaje = "Categoría eliminada correctamente";
        }else{
            $this->view->mensaje = "No se pudo eliminar la categoría";
        }
        $this->render();
    }
}
?>
<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 10:55 AM
 */
class Clientes extends controller
{
    function __construct(){
        parent::__construct();
        $this->view->mensaje = "";
    }
    function render(){
        $clientes = $this->view->datos = $this->model->get();

        $this->view->clientes = $clientes;
        $this->view->render('clientes/index');
    }
    function verCliente($param = null){
        $id = $param[0];
        $cliente = $this->model->getById($id);
        session_start();
        $_SESSION["id"] = $cliente->id;
        $this->view->cliente = $cliente;
        $this->view->render('clientes/detalle');
    }

    function crearCliente(){
        $this->view->render('clientes/crear');
    }
    function guardarCliente(){
        $nombre    = $_POST['nombre'];
        $direccion  = $_POST['direccion'];
        $telefono  = $_POST['telefono'];
        $nacionalidad  = $_POST['nacionalidad'];
        $edad  = $_POST['edad'];
        $email  = $_POST['email'];
        if($this->model->insert([
                                'nombre' => $nombre,
                                'direccion' => $direccion,
                                'telefono' => $telefono,
                                'nacionalidad' => $nacionalidad,
                                'edad' => $edad,
                                'email' => $email
                                ]))
        {
            $this->view->mensaje = "Cliente creado correctamente";

            $this->render();
        }else{
            $this->view->mensaje = "El cliente ya existe";
            $this->view->render('clientes/crear');
        }
    }
    function actualizarCliente($param = null){

        $id = $_POST['id'];
        $nombre    = $_POST['nombre'];
        $direccion  = $_POST['direccion'];
        $telefono  = $_POST['telefono'];
        $nacionalidad  = $_POST['nacionalidad'];
        $edad  = $_POST['edad'];
        $email  = $_POST['email'];

        if($this->model->update([
                                'id' => $id,
                                'nombre' => $nombre,
                                'direccion' => $direccion,
                                'telefono' => $telefono,
                                'nacionalidad' => $nacionalidad,
                                'edad' => $edad,
                                'email' => $email]))
        {
            $cliente = new EntityClientes();
            $cliente->id = $id;
            $cliente->nombre = $nombre;
            $cliente->direccion = $direccion;
            $cliente->telefono = $telefono;
            $cliente->nacionalidad = $nacionalidad;
            $cliente->edad = $edad;
            $cliente->email = $email;

            $this->view->cliente = $cliente;
            $this->view->mensaje = "Cliente actualizado correctamente";
        }else{
            $this->view->mensaje = "No se pudo actualizar al Cliente";
        }
        $this->view->render('clientes/detalle');
    }
    function eliminarCliente($param = null){
        $id = $param[0];
        if($this->model->delete($id)){
            $this->view->mensaje = "Cliente eliminado correctamente";
        }else{
            $this->view->mensaje = "No se pudo eliminar al Cliente";
        }
        $this->render();
    }

}
?>
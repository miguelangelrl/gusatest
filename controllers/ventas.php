<?php
/**
 * Created by
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 10:55 AM
 */

require_once 'models/clientesmodel.php';
require_once 'models/asesoresmodel.php';
require_once 'models/productosmodel.php';

class Ventas extends controller
{
    function __construct(){
        parent::__construct();
        $this->view->mensaje = "";
    }
    function render(){
        $ventas = $this->model->get();

        $this->view->ventas = $ventas;
        $this->view->render('ventas/index');
    }


    function verVenta($param = null){
        $c = new ClientesModel();
        $clientes = $c->get();

        $a = new AsesoresModel();
        $asesores = $a ->get();

        $p = new ProductosModel();
        $productos = $p->get();

        $id = $param[0];
        $venta = $this->model->getById($id);


        $det = json_decode($venta->detalles);

        $venta->detalles = $det;


        $this->view->data = array('venta'=>$venta, 'clientes' => $clientes, 'asesores' => $asesores, 'productos' => $productos);

        $this->view->render('ventas/detalle');
    }

    function crearVenta(){
        $c = new ClientesModel();
        $clientes = $c->get();

        $a = new AsesoresModel();
        $asesores = $a ->get();

        $p = new ProductosModel();
        $productos = $p->get();

        $this->view->data = array('clientes' => $clientes, 'asesores' => $asesores, 'productos' => $productos);

        $this->view->render('ventas/crear');
    }
    function guardarVenta(){


        $titulo    = $_POST['titulo'];
        $clientes_id  = $_POST['clientes_id'];
        $asesores_id  = $_POST['asesores_id'];
        $comentarios  = $_POST['comentarios'];
        $total  = $_POST['total_venta'];
        $fecha_creacion  = date('Y/m/d');
        $fecha_actualizacion  = date('Y/m/d');
        $estatus = $_POST['estatus'];

        for($i=0; $i<=count($_POST['products_id'])-1; $i++){

            $data[$i] = array(
                                'products_id' => $_POST['products_id'][$i],
                                'products_nombre' => $_POST['products_nombre'][$i],
                                'products_descripcion' => $_POST['products_descripcion'][$i],
                                'products_precio' => $_POST['products_precio'][$i],
                                'products_cantidad' => $_POST['products_cantidad'][$i],
                                'products_subtotal' => $_POST['products_subtotal'][$i]
                             );
        }

        $convert = json_encode($data);
        $detalles = $convert;

        if($this->model->insert([
            'titulo' => $titulo,
            'clientes_id' => $clientes_id,
            'detalles' => $detalles,
            'total' => $total,
            'asesores_id' => $asesores_id,
            'comentarios' => $comentarios,
            'fecha_creacion' => $fecha_creacion,
            'fecha_actualizacion' => $fecha_actualizacion,
            'estatus' => $estatus
        ]))
        {
            $this->view->mensaje = "Venta agregada correctamente";

            $this->render();
        }else{
            $this->view->mensaje = "Hubo un error verifique los datos ingresados";
            $this->view->render('ventas/crear');
        }
    }
    function actualizarVenta($param = null){

        $id    = $_POST['venta_id'];
        $titulo    = $_POST['titulo'];
        $clientes_id  = $_POST['clientes_id'];
        $asesores_id  = $_POST['asesores_id'];
        $comentarios  = $_POST['comentarios'];
        $total  = $_POST['total_venta'];
        $estatus  = $_POST['estatus'];


        for($i=0; $i<=count($_POST['products_id'])-1; $i++){

            $data[$i] = array(
                'products_id' => $_POST['products_id'][$i],
                'products_nombre' => $_POST['products_nombre'][$i],
                'products_descripcion' => $_POST['products_descripcion'][$i],
                'products_precio' => $_POST['products_precio'][$i],
                'products_cantidad' => $_POST['products_cantidad'][$i],
                'products_subtotal' => $_POST['products_subtotal'][$i]
            );
        }

        $convert = json_encode($data);
        $detalles = $convert;

        if($this->model->update([
            'id' => $id,
            'titulo' => $titulo,
            'clientes_id' => $clientes_id,
            'detalles' => $detalles,
            'total' => $total,
            'asesores_id' => $asesores_id,
            'comentarios' => $comentarios,
            'fecha_actualizacion' => date("Y/m/d"),
            'estatus' => $estatus]))
        {
            $venta = $this->model->getById($id);


            $det = json_decode($venta->detalles);

            $venta->detalles = $det;

            $c = new ClientesModel();
            $clientes = $c->get();

            $a = new AsesoresModel();
            $asesores = $a ->get();

            $p = new ProductosModel();
            $productos = $p->get();

            $this->view->data = array('venta'=>$venta, 'clientes' => $clientes, 'asesores' => $asesores, 'productos' => $productos);

            $this->view->mensaje = "Venta actualizada correctamente";
        }else{
            $this->view->mensaje = "No se pudo actualizar el Producto";
        }
        $this->view->render('ventas/detalle');
    }
    function eliminarVenta($param = null){
        $id = $param[0];
        if($this->model->delete($id)){
            $this->view->mensaje = "Venta eliminada correctamente";
        }else{
            $this->view->mensaje = "No se pudo eliminar";
        }
        $this->render();
    }

}
?>
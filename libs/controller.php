<?php
/**
 * Created by:
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 08:05 AM
 */

class controller
{
    function __construct()
    {

        $this->view = new view();
    }

    function loadModel($model){
        $url = 'models/'.$model.'model.php';

        if(file_exists($url)){
            require $url;

            $modelName = $model.'Model';

            $this->model = new $modelName();
        }
    }
}
?>
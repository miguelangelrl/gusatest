<?php
/**
 * Created by:
 * @User: Miguel Ángel Ramírez López
 * @Email: ing.miguelangel.rl@gmail.com
 * Date: 08/02/2019
 * Time: 08:05 AM
 */

class View
{
    function __construct()
    {

    }

    function render($nombre){
        require 'views/' . $nombre . '.php';
    }
}
?>